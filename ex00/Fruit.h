/*
** Fruit.h for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d14m/ex00
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Tue Jan 21 11:02:26 2014 Jean Gravier
** Last update Tue Jan 21 11:23:38 2014 Jean Gravier
*/

#ifndef _FRUIT_H_
#define _FRUIT_H_

#include <string>

class			Fruit
{
 public:
  Fruit();
  virtual ~Fruit() = 0;

 public:
  std::string const&	getName() const;
  int			getVitamins() const;

 protected:
  std::string		_name;
  int			_vitamins;
};

#endif /* _FRUIT_H_ */
