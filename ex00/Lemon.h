/*
** Lemon.h for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d14m/ex00
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Tue Jan 21 11:15:22 2014 Jean Gravier
** Last update Tue Jan 21 11:16:40 2014 Jean Gravier
*/

#ifndef _LEMON_H_
#define _LEMON_H_

#include "Fruit.h"
#include <string>

class			Lemon: public Fruit
{
 public:
  Lemon();
  ~Lemon();
};

#endif /* _LEMON_H_ */
