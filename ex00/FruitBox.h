/*
** FruitBox.h for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d14m/ex00
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Tue Jan 21 11:24:51 2014 Jean Gravier
** Last update Tue Jan 21 12:54:55 2014 Jean Gravier
*/

#ifndef _FRUITBOX_H_
#define _FRUITBOX_H_

#include "FruitNode.h"
#include <utility>
#include <list>

class			FruitBox
{
 public:
  FruitBox(int);
  ~FruitBox();

 public:
  int			nbFruits() const;
  bool			putFruit(Fruit*);
  Fruit*		pickFruit();
  FruitNode*		head() const;

 private:
  std::list<FruitNode*> _fruitList;
  int			_size;
  int			_nbFruits;
};

#endif /* _FRUITBOX_H_ */
