//
// Fruit.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d14m/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Tue Jan 21 11:02:23 2014 Jean Gravier
// Last update Tue Jan 21 11:12:19 2014 Jean Gravier
//

#include "Fruit.h"
#include <string>

Fruit::Fruit()
{
  this->_name = "";
  this->_vitamins = 0;
}

Fruit::~Fruit()
{

}

std::string const&	Fruit::getName() const
{
  return (this->_name);
}

int			Fruit::getVitamins() const
{
  return(this->_vitamins);
}

