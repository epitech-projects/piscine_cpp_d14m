//
// FruitBox.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d14m/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Tue Jan 21 11:24:48 2014 Jean Gravier
// Last update Tue Jan 21 13:27:52 2014 Jean Gravier
//

#include "FruitBox.h"
#include "FruitNode.h"
#include "Fruit.h"
#include <utility>
#include <list>

FruitBox::FruitBox(int size): _size(size)
{
  this->_nbFruits = 0;
}

FruitBox::~FruitBox()
{

}

FruitNode::FruitNode(Fruit* fruit)
{
  this->_fruit = fruit;
  this->_next = NULL;
}

FruitNode*	FruitNode::next() const
{
  return (this->_next);
}

void		FruitNode::setNext(FruitNode* next)
{
  this->_next = next;
}

Fruit*		FruitNode::getFruit() const
{
  return (this->_fruit);
}

int		FruitBox::nbFruits() const
{
  return (this->_nbFruits);
}

bool				FruitBox::putFruit(Fruit* fruit)
{
  FruitNode			node(fruit);
  std::list<FruitNode*>::iterator	it;

  if (this->_size >= this->_nbFruits)
    return (false);
  else
    {
      for (it = this->_fruitList.begin(); it != this->_fruitList.end(); ++it)
	{
	  if ((*it)->getFruit() == fruit)
	    return (false);
	}
      this->_fruitList.push_back(&node);
      node.setNext(this->_fruitList.back());
      return (true);
    }
}

Fruit*		FruitBox::pickFruit()
{
  FruitNode	*temp;

  if (this->_size)
    {
      temp = this->_fruitList.front();
      this->_fruitList.pop_front();
      return (temp->getFruit());
    }
  else
    return (NULL);
}

FruitNode*	FruitBox::head() const
{
  if (this->_size)
    return (this->_fruitList.front());
  else
    return (NULL);
}
