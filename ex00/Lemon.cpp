//
// Lemon.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d14m/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Tue Jan 21 11:15:14 2014 Jean Gravier
// Last update Tue Jan 21 11:19:28 2014 Jean Gravier
//

#include "Lemon.h"

Lemon::Lemon(): Fruit()
{
  this->_name = "lemon";
  this->_vitamins = 3;
}

Lemon::~Lemon()
{

}
