/*
** FruitNode.h for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d14m/ex00
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Tue Jan 21 12:29:01 2014 Jean Gravier
** Last update Tue Jan 21 13:28:22 2014 Jean Gravier
*/

#ifndef _FRUITNODE_H_
#define _FRUITNODE_H_

#include "Fruit.h"
#include <list>
//#include <utility>

class			FruitNode
{
 public:
  FruitNode(Fruit*);
  ~FruitNode() {};

 public:
  Fruit*		getFruit() const;
  FruitNode*		next() const;
  void			setNext(FruitNode*);

 private:
  Fruit*		_fruit;
  FruitNode*		_next;
};

#endif /* _FRUITNODE_H_ */
